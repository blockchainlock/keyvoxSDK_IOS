# BCLBle -- SDK for operating BCL Bluetooth hardware devices

All APIs can refer to：<BCLBle/BCLBleModule.framework/Versions/A/Headers/BCLBle.h>

> Every API method has an annotation


Demo(Objective-C) can refer to: BCLBleSDKSample/BCLBleSDKSample-OC

Demo(Swift) can refer to: BCLBleSDKSample/BCLBleSDKSample-Swift

## Installation

`pod 'BCLBleModule', :path => '${BCLBleModule.podspec's path}`

## The calling logic is as follows
```flow
1.Scan
/**
 BLEスキャン(MACフィルター)
 Scan(Filtter by mac address)
 */
+ (void)scan:(NSString * _Nullable )channel 
		 mac:(NSString *)mac 
	   block:(void (^)(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI))block;
	
2.Connect
/**
 BLE接続(直接)
 Connect(Direct connection via CBPeripheral)
 */
+ (void)connect:(NSString * _Nullable )channel 
     peripheral:(CBPeripheral * _Nullable )peripheral 
	      block:(void (^)(CBCentralManager * _Nullable central, CBPeripheral * _Nullable peripheral))block;

3.Login
/**
 ログイン
 Login command
 */
+ (void)login:(NSString * _Nullable )channel
		  mac:(NSString *)mac
   peripheral:(CBPeripheral * _Nullable )peripheral
	 masterId:(NSString *)masterId
		  pwd:(NSString *)pwd
		block:(void (^)(BOOL logined, CBPeripheral * _Nonnull peripheral, NSString * _Nonnull channel, BOOL timeout, NSString *masterId, NSString *userId))block;
```
`We recommend calling the login method directly, because the connection status of the device is judged in the login method. `

`If it is not connected, it will give priority to the connection and then send the login command.`

***Then you can call these apis***

###### 1. Unlock Bluetooth hardware device
Objective-C:
```objc
[BCLBle unlock:CHANNELID block:^(BOOL isCorrect, NSString * _Nonnull key, BOOL timeout) {
	if (isCorrect) {
		DDLogVerbose(@"[BLEDemo] Unlock succeed");
	}else {
		DDLogError(@"[BLEDemo] Unlock failed");
	}
}];
```
Swift:
```swift
BCLBle.unlock(CHANNELID) { (isCorrect, key, timeout) in
	if isCorrect {
		DDLogVerbose("[BLEDemo] Unlock succeed");
	}else {
		DDLogError("[BLEDemo] Unlock failed");
	}
}
```

###### 2. Lock Bluetooth hardware device
Objective-C:
```objc
[BCLBle lock:CHANNELID block:^(BOOL isCorrect, NSString * _Nonnull key, BOOL timeout) {
	if (isCorrect) {
		DDLogVerbose(@"[BLEDemo] Lock succeed");
	}else {
		DDLogError(@"[BLEDemo] Lock failed");
	}
}];
```
Swift:
```swift
BCLBle.lock(CHANNELID) { (isCorrect, key, timeout) in
	if isCorrect {
		DDLogVerbose("[BLEDemo] Lock succeed");
	}else {
		DDLogError("[BLEDemo] Lock failed");
	}
}
```

###### 3. Get lock's status(Current is unlocked or locked)
Objective-C:
```objc
[BCLBle lockStatus:^(BOOL isCorrect, NSString * _Nonnull type) {
	if (isCorrect) {
		if ([type intValue] == 1 || [type intValue] == 2) {
			DDLogVerbose(@"[BLEDemo] current lock's status -- Unlocked");
		}else if ([type intValue] == 3) {
			DDLogVerbose(@"[BLEDemo] current lock's status -- Locked");
		}else {
			DDLogVerbose(@"[BLEDemo] current lock's status -- Unkown");
		}
	}else {
		DDLogError(@"[BLEDemo] Get current lock's status failed");
	}
}];
```
Swift:
```swift
BCLBle.lockStatus { (isCorrect, type: String!) in
	if isCorrect {
		if Int(type) == 1 || Int(type) == 2 {
			DDLogVerbose("[BLEDemo] current lock's status -- Unlocked")
		}else if Int(type) == 3 {
			DDLogVerbose("[BLEDemo] current lock's status -- Locked")
		}else {
			DDLogVerbose("[BLEDemo] current lock's status -- Unkown")
		}
	}else {
		DDLogError("[BLEDemo] Get current lock's status failed")
	}
}
```

###### 4. Get lock's battery
Objective-C:
```objc
[BCLBle lockBattery:^(BOOL isCorrect, NSString *battery) {
	if (isCorrect) {
		DDLogVerbose(@"[BLEDemo] Current lock's battery -- %@",battery?:@"Unkonw");
	}else {
		DDLogError(@"[BLEDemo] Get current lock's battery failed");
	}
}];
```
Swift:
```swift
BCLBle.lockBattery { (isCorrect, battery) in
	if isCorrect {
		DDLogVerbose("[BLEDemo] Current lock's battery -- \(battery)")
	}else {
		DDLogError("[BLEDemo] Get current lock's battery failed")
	}
}
```

###### 5. Add lock's user
Objective-C:
```objc
[BCLBle addUserWithMasterId:TEST_MASTERID userId:TEST_USERID startTime:nil endTime:nil times:nil pwdType:TEST_USER_TYPE pwd:TEST_USER_PWD1 block:^(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId) {
	if (isCorrect) {
		DDLogVerbose(@"[BLEDemo](User add) -- isCorrect: %@", isCorrect?@"true":@"false");
		DDLogVerbose(@"[BLEDemo](User add) -- resultInt: %d", resultInt);
		DDLogVerbose(@"[BLEDemo](User add) -- msg: %@", msg);
		DDLogVerbose(@"[BLEDemo](User add) -- superId: %@", masterId);
		DDLogVerbose(@"[BLEDemo](User add) -- userId: %@", userId);
	}else {
		DDLogError(@"[BLEDemo](User add) -- isCorrect: %@", isCorrect?@"true":@"false");
		DDLogError(@"[BLEDemo](User add) -- resultInt: %d", resultInt);
		DDLogError(@"[BLEDemo](User add) -- msg: %@", msg);
		DDLogError(@"[BLEDemo](User add) -- superId: %@", masterId);
		DDLogError(@"[BLEDemo](User add) -- userId: %@", userId);
	}
}];
```
Swift:
```swift
BCLBle.addUser(withMasterId: TEST_MASTERID, userId: TEST_USERID, startTime: nil, endTime: nil, times: nil, pwdType: TEST_USER_TYPE, pwd: TEST_USER_PWD1) { (isCorrect, resultInt, msg, masterId, userId) in
	if isCorrect {
		DDLogVerbose("[BLEDemo](User add) -- isCorrect: \(isCorrect ? "true" : "false")")
		DDLogVerbose("[BLEDemo](User add) -- resultInt: \(resultInt)")
		DDLogVerbose("[BLEDemo](User add) -- msg: \(msg)")
		DDLogVerbose("[BLEDemo](User add) -- superId: \(masterId)")
		DDLogVerbose("[BLEDemo](User add) -- userId: \(userId)")
	}else {
		DDLogError("[BLEDemo](User add) -- isCorrect: \(isCorrect ? "true" : "false")")
		DDLogError("[BLEDemo](User add) -- resultInt: \(resultInt)")
		DDLogError("[BLEDemo](User add) -- msg: \(msg)")
		DDLogError("[BLEDemo](User add) -- superId: \(masterId)")
		DDLogError("[BLEDemo](User add) -- userId: \(userId)")
	}
}
```

###### 6. Modify lock's user
Objective-C:
```objc
[BCLBle modifyUserWithMasterId:TEST_MASTERID userId:TEST_USERID startTime:nil endTime:nil times:nil pwdType:TEST_USER_TYPE pwd:TEST_USER_PWD2 block:^(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId) {
	if (isCorrect) {
		DDLogVerbose(@"[BLEDemo](User modify) -- isCorrect: %@", isCorrect?@"true":@"false");
		DDLogVerbose(@"[BLEDemo](User modify) -- resultInt: %d", resultInt);
		DDLogVerbose(@"[BLEDemo](User modify) -- msg: %@", msg);
		DDLogVerbose(@"[BLEDemo](User modify) -- superId: %@", masterId);
		DDLogVerbose(@"[BLEDemo](User modify) -- userId: %@", userId);
	}else {
		DDLogError(@"[BLEDemo](User modify) -- isCorrect: %@", isCorrect?@"true":@"false");
		DDLogError(@"[BLEDemo](User modify) -- resultInt: %d", resultInt);
		DDLogError(@"[BLEDemo](User modify) -- msg: %@", msg);
		DDLogError(@"[BLEDemo](User modify) -- superId: %@", masterId);
		DDLogError(@"[BLEDemo](User modify) -- userId: %@", userId);
	}
}];
```
Swift:
```swift
BCLBle.modifyUser(withMasterId: TEST_MASTERID, userId: TEST_USERID, startTime: nil, endTime: nil, times: nil, pwdType: TEST_USER_TYPE, pwd: TEST_USER_PWD2) { (isCorrect, resultInt, msg, masterId, userId) in
	if isCorrect {
		DDLogVerbose("[BLEDemo](User modify) -- isCorrect: \(isCorrect ? "true" : "false")")
		DDLogVerbose("[BLEDemo](User modify) -- resultInt: \(resultInt)")
		DDLogVerbose("[BLEDemo](User modify) -- msg: \(msg)")
		DDLogVerbose("[BLEDemo](User modify) -- superId: \(masterId)")
		DDLogVerbose("[BLEDemo](User modify) -- userId: \(userId)")
	}else {
		DDLogError("[BLEDemo](User modify) -- isCorrect: \(isCorrect ? "true" : "false")")
		DDLogError("[BLEDemo](User modify) -- resultInt: \(resultInt)")
		DDLogError("[BLEDemo](User modify) -- msg: \(msg)")
		DDLogError("[BLEDemo](User modify) -- superId: \(masterId)")
		DDLogError("[BLEDemo](User modify) -- userId: \(userId)")
	}
}
```

###### 7. Delete lock's user
Objective-C:
```objc
[BCLBle deleteUserWithMasterId:TEST_MASTERID userId:TEST_USERID block:^(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId) {
	if (isCorrect) {
		DDLogVerbose(@"[BLEDemo](User delete) -- isCorrect: %@", isCorrect?@"true":@"false");
		DDLogVerbose(@"[BLEDemo](User delete) -- resultInt: %d", resultInt);
		DDLogVerbose(@"[BLEDemo](User delete) -- msg: %@", msg);
		DDLogVerbose(@"[BLEDemo](User delete) -- superId: %@", masterId);
		DDLogVerbose(@"[BLEDemo](User delete) -- userId: %@", userId);
	}else {
		DDLogError(@"[BLEDemo](User delete) -- isCorrect: %@", isCorrect?@"true":@"false");
		DDLogError(@"[BLEDemo](User delete) -- resultInt: %d", resultInt);
		DDLogError(@"[BLEDemo](User delete) -- msg: %@", msg);
		DDLogError(@"[BLEDemo](User delete) -- superId: %@", masterId);
		DDLogError(@"[BLEDemo](User delete) -- userId: %@", userId);
	}
}];
```
Swift:
```swift
BCLBle.deleteUser(withMasterId: TEST_MASTERID, userId: TEST_USERID) { (isCorrect, resultInt, msg, masterId, userId) in
	if isCorrect {
		DDLogVerbose("[BLEDemo](User delete) -- isCorrect: \(isCorrect ? "true" : "false")")
		DDLogVerbose("[BLEDemo](User delete) -- resultInt: \(resultInt)")
		DDLogVerbose("[BLEDemo](User delete) -- msg: \(msg)")
		DDLogVerbose("[BLEDemo](User delete) -- superId: \(masterId)")
		DDLogVerbose("[BLEDemo](User delete) -- userId: \(userId)")
	}else {
		DDLogError("[BLEDemo](User delete) -- isCorrect: \(isCorrect ? "true" : "false")")
		DDLogError("[BLEDemo](User delete) -- resultInt: \(resultInt)")
		DDLogError("[BLEDemo](User delete) -- msg: \(msg)")
		DDLogError("[BLEDemo](User delete) -- superId: \(masterId)")
		DDLogError("[BLEDemo](User delete) -- userId: \(userId)")
	}
}
```
