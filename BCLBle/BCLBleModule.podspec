Pod::Spec.new do |s|
  s.name  = 'BCLBleModule'
  s.version = '1.0.2'
  s.summary = 'BCLBleModule By Linkage'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/LKG-TEAM/BCLBle.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE'}
  s.author           = { 'linkstec@linkstec.com' => 'linkstec@linkstec.com' }
  s.source = { :path => '.' }

  s.ios.deployment_target = '8.0'
  s.ios.vendored_frameworks = 'BCLBleModule.framework'
  s.libraries  = 'z'
end
