//
//  BCLBleManager.h
//  BabyBluetooth
//
//  Created by pantao on 2018/10/18.
//

#import <Foundation/Foundation.h>
@class CBCentralManager;
@class CBPeripheral;

NS_ASSUME_NONNULL_BEGIN

extern NSString * const BCLBLE_NOTIFICATION_DISCONNECTED;// Bluetooth disconnected notification
extern NSString * const BCLBLE_NOTIFICATION_CONNECTED;// Bluetooth connected notification
extern NSString * const BCLBLE_NOTIFICATION_CONNECTFAILED;// Bluetooth connect but failed notification

@interface BCLBle : NSObject

/**
 蓝牙扫描(mac地址来过滤)
 Scan(Filtter by mac address)
 */
+ (void)scan:(NSString * _Nullable )channel mac:(NSString *)mac block:(void (^)(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI))block;

/**
 蓝牙扫描(所有)
 Scan(For all devices)
 */
+ (void)scan:(NSString * _Nullable )channel block:(void (^)(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI))block;

/**
 停止扫描
 Scan stop
 */
+ (void)scanCancel;

/**
 蓝牙连接(直连)
 Connect(Direct connection via CBPeripheral)
 */
+ (void)connect:(NSString * _Nullable )channel peripheral:(CBPeripheral * _Nullable )peripheral block:(void (^)(CBCentralManager * _Nullable central, CBPeripheral * _Nullable peripheral))block;

/**
 设置发现设service的Characteristics的委托
 Set the callback of the characteristics of the discovery service
 */
+ (void)onDiscoverCharacteristicsWithChannel:(NSString * _Nullable )channel targetPeripheral:(CBPeripheral *)targetPeripheral block:(void (^)(void))block;

/**
 蓝牙断开连接(通过制定CBPeripheral)
 DisConnect(Direct disconnection via CBPeripheral)
 */
+ (void)disConnectBleWithPeripheral:(CBPeripheral *)peripheral;

/**
 开锁指令
 Open lock command(Must be executed in the callback block of the login command)
 */
+ (void)unlock:(NSString * _Nullable )channel block:(void (^)(BOOL isCorrect, NSString *channel, BOOL timeout))block;

/**
 落锁指令
 Close lock command(Must be executed in the callback block of the login command)
 */
+ (void)lock:(NSString * _Nullable )channel block:(void (^)(BOOL isCorrect, NSString *channel, BOOL timeout))block;

/**
 登录
 Login command
 mac:       Device's mac address
 masterId:  2-digit string, Usually a string of '00'
 pwd:       Password
 */
+ (void)login:(NSString * _Nullable )channel
          mac:(NSString *)mac
   peripheral:(CBPeripheral * _Nullable )peripheral
     masterId:(NSString *)masterId
          pwd:(NSString *)pwd
        block:(void (^)(BOOL logined, CBPeripheral * _Nonnull peripheral, NSString * _Nonnull channel, BOOL timeout, NSString *masterId, NSString *userId))block;

/**
 获取锁具状态(当前是开着/关着)
 Get lock's status(Current is unlocked or locked)
 type:
    1 or 2: unlocked
    3:  locked
 */
+ (void)lockStatus:(void (^)(BOOL isCorrect, NSString *type))block;

/**
 获取锁具电量
 Get lock's battery
 battery: lock's battery
 */
+ (void)lockBattery:(void (^)(BOOL isCorrect, NSString * _Nonnull battery))block;

/**
 * 增加用户
 * Add user
 * masterId:  2-digit string
 * userId:    2-digit string. The lock automatically generates userId when userId = nil.
 * startTime: The start time(UTC). The unit is seconds, eg. [NSString stringWithFormat:@"%ld",[[NSDate date] timeIntervalSince1970]]
 * endTime:   The end time(UTC). The unit is seconds, eg. [NSString stringWithFormat:@"%ld",[[NSDate date] timeIntervalSince1970]]. If endTime = nil, then no limit.
 * times:     Usage count. No limit when times = nil.
 * pwdType:   The type of password
 *            1 --> Owner, length is 12-digit(Can only modify but not add)
 *            2 --> Master, length is 10-digit(Can only modify but not add)
 *            3 --> IC+PIN, length is 13-digit
 *            4 --> IC, length is 16-digit
 *            5 --> General user, length is 4-digit ~ 8-digit
 *            6 --> One-time password, length is 11-digit
 * pwd:       Password
 */
+ (void)addUserWithMasterId:(NSString *)masterId
                     userId:(NSString * _Nullable)userId
                  startTime:(NSString * _Nullable)startTime
                    endTime:(NSString * _Nullable)endTime
                      times:(NSString * _Nullable)times
                    pwdType:(NSInteger)pwdType
                        pwd:(NSString *)pwd
                      block:(void (^)(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId))block;

/**
 * 修改用户
 * Modify user
 * masterId:  2-digit string
 * userId:    2-digit string
 * startTime: The start time(UTC). The unit is seconds, eg. [NSString stringWithFormat:@"%ld",[[NSDate date] timeIntervalSince1970]]
 * endTime:   The end time(UTC). The unit is seconds, eg. [NSString stringWithFormat:@"%ld",[[NSDate date] timeIntervalSince1970]]. If endTime = nil, then no limit.
 * times:     Usage count. No limit when times = nil.
 * pwdType:   The type of password
 *            1 --> Owner, length is 12-digit(Can only modify but not add)
 *            2 --> Master, length is 10-digit(Can only modify but not add)
 *            3 --> IC+PIN, length is 13-digit
 *            4 --> IC, length is 16-digit
 *            5 --> General user, length is 4-digit ~ 8-digit
 *            6 --> One-time password, length is 11-digit
 * pwd:       Password
 */
+ (void)modifyUserWithMasterId:(NSString *)masterId
                        userId:(NSString *)userId
                     startTime:(NSString * _Nullable)startTime
                       endTime:(NSString * _Nullable)endTime
                         times:(NSString * _Nullable)times
                       pwdType:(NSInteger)pwdType
                           pwd:(NSString *)pwd
                         block:(void (^)(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId))block;

/**
 * 删除用户
 * Delete user
 * masterId:  2-digit string
 * userId:    2-digit string
 */
+ (void)deleteUserWithMasterId:(NSString *)masterId
                        userId:(NSString *)userId
                         block:(void (^)(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId))block;

@end

NS_ASSUME_NONNULL_END
