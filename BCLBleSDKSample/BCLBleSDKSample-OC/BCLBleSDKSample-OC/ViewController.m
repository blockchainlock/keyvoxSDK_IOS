//
//  ViewController.m
//  BCLBleSDKSample-OC
//
//  Created by pantao on 2021/3/15.
//

#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "BCLUtil.h"
#import <BCLBleModule/BCLBle.h>
@import IQKeyboardManager;

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "DDLog.h"
#endif

#ifdef DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_WARN;
#endif

#define CHANNELID @"channel_test"
#define TEST_MASTERID @"00"
#define TEST_USERID @"11"
#define TEST_USER_TYPE 5
#define TEST_USER_PWD1 @"0001"
#define TEST_USER_PWD2 @"66666666"

@interface ViewController ()<DDLogger>

@property (weak, nonatomic) IBOutlet UITextField *macTF;
@property (weak, nonatomic) IBOutlet UITextField *idTF;
@property (weak, nonatomic) IBOutlet UITextField *pwdTF;
@property (weak, nonatomic) IBOutlet UITextView *logTV;
@property (weak, nonatomic) IBOutlet UILabel *statusL;

@property (strong, nonatomic) __block CBPeripheral *peripheral;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [BCLUtil bcl_colorWithHex:0x00AFEC];
    self.logTV.editable = NO;
    // 启用IQKeyboardManager
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [DDLog addLogger:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ble_connected:) name:BCLBLE_NOTIFICATION_CONNECTED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ble_disconnected:) name:BCLBLE_NOTIFICATION_DISCONNECTED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ble_connectfailed:) name:BCLBLE_NOTIFICATION_CONNECTFAILED object:nil];
}

- (void)ble_connected:(NSNotification *)n
{
    NSDictionary *obj = [n object];
    CBPeripheral *peripheral = [obj objectForKey:@"peripheral"];
    DDLogVerbose(@"[BLEDemo] %@: %@", peripheral.name, @"连接成功");
    self.view.backgroundColor = [BCLUtil bcl_colorWithHex:0x88cb7f];
    
    if (self.peripheral && peripheral && ![self.peripheral isEqual:peripheral]) {
        DDLogError(@"[BLEDemo] %@: %@", peripheral.name, @"连接成功的不是当前要连接的peripheral");
    }
}

- (void)ble_disconnected:(NSNotification *)n
{
    NSDictionary *obj = [n object];
    CBPeripheral *peripheral = [obj objectForKey:@"peripheral"];
    DDLogError(@"[BLEDemo] %@: %@", peripheral.name, @"连接断开");
    self.view.backgroundColor = [BCLUtil bcl_colorWithHex:0xa25768];
}

- (void)ble_connectfailed:(NSNotification *)n
{
    NSDictionary *obj = [n object];
    CBPeripheral *peripheral = [obj objectForKey:@"peripheral"];
    DDLogDebug(@"[BLEDemo] %@:%@", peripheral.name, @"连接失败");
    self.view.backgroundColor = [BCLUtil bcl_colorWithHex:0x88cb7f];
}

- (IBAction)ble_scan:(UIButton *)sender {
    DDLogDebug(@"[BLEDemo] Scanning...");
    
    NSString *macInput = self.macTF.text;
    macInput = [macInput stringByReplacingOccurrencesOfString:@" " withString:@""];
    macInput = [macInput stringByReplacingOccurrencesOfString:@":" withString:@""];
    if (macInput.length <= 0) {
        DDLogDebug(@"[BLEDemo] Input mac address plz");
        NSLog(@"Input mac address plz");
        return;
    }
    
    [BCLBle scan:CHANNELID mac:macInput block:^(CBCentralManager * _Nonnull central, CBPeripheral * _Nonnull peripheral, NSDictionary * _Nonnull advertisementData, NSNumber * _Nonnull RSSI) {
        DDLogVerbose(@"[BLEDemo] Scaned mac address: %@ (RSSI: %@)", macInput, RSSI);
        NSLog(@"Scaned mac address: %@ (RSSI: %@)", macInput, RSSI);
        DDLogVerbose(@"[BLEDemo] Device found: %@\nExtended data: %@",peripheral.name,advertisementData);
        NSLog(@"[BLEDemo] Device found: %@\nExtended data: %@",peripheral.name,advertisementData);
        [BCLBle scanCancel];
        
        self.peripheral = peripheral;
    }];
}

- (IBAction)ble_stopscan:(id)sender {
    [BCLBle scanCancel];
}

- (IBAction)ble_connect:(UIButton *)sender {
    DDLogDebug(@"[BLEDemo] Connecting...");
    
    NSString *macInput = self.macTF.text;
    macInput = [macInput stringByReplacingOccurrencesOfString:@" " withString:@""];
    macInput = [macInput stringByReplacingOccurrencesOfString:@":" withString:@""];
    if (macInput.length <= 0) {
        DDLogDebug(@"[BLEDemo] Input mac address plz");
        NSLog(@"Input mac address plz");
        return;
    }
    
    [BCLBle connect:CHANNELID peripheral:self.peripheral block:^(CBCentralManager * _Nullable central, CBPeripheral * _Nullable peripheral) {
        if (peripheral) {
            [BCLBle onDiscoverCharacteristicsWithChannel:CHANNELID targetPeripheral:self.peripheral block:^{
                
            }];
        }
    }];
}

- (IBAction)ble_disconnect:(id)sender {
    [BCLBle disConnectBleWithPeripheral:self.peripheral];
}

- (IBAction)ble_login:(id)sender {
    if (!self.idTF.text || self.idTF.text.length == 0) {
        DDLogError(@"[BLEDemo] Please type in master id");
        return;
    }
    if (!self.pwdTF.text || self.pwdTF.text.length == 0) {
        DDLogError(@"[BLEDemo] Please type in password");
        return;
    }
    DDLogDebug(@"[BLEDemo] Logining...");
    
    NSString *macInput = self.macTF.text;
    macInput = [macInput stringByReplacingOccurrencesOfString:@" " withString:@""];
    macInput = [macInput stringByReplacingOccurrencesOfString:@":" withString:@""];
    [BCLBle login:CHANNELID mac:macInput peripheral:self.peripheral masterId:self.idTF.text pwd:self.pwdTF.text block:^(BOOL logined, CBPeripheral * _Nonnull peripheral, NSString * _Nonnull channel, BOOL timeout, NSString *masterId, NSString *userId) {
        self.peripheral = peripheral;
        if (logined) {
            DDLogVerbose(@"[BLEDemo] Login succeed");
            DDLogVerbose(@"[BLEDemo] Login succeed masterId: %@",masterId);
            DDLogVerbose(@"[BLEDemo] Login succeed userId: %@",userId);
        }else {
            if (timeout) {
                DDLogError(@"[BLEDemo] Login timeout");
            }else {
                DDLogError(@"[BLEDemo] Login failed");
            }
        }
    }];
}

- (IBAction)ble_open:(id)sender {
    [BCLBle unlock:CHANNELID block:^(BOOL isCorrect, NSString * _Nonnull key, BOOL timeout) {
        if (isCorrect) {
            DDLogVerbose(@"[BLEDemo] Unlock succeed");
        }else {
            DDLogError(@"[BLEDemo] Unlock failed");
        }
    }];
}

- (IBAction)ble_close:(id)sender {
    [BCLBle lock:CHANNELID block:^(BOOL isCorrect, NSString * _Nonnull key, BOOL timeout) {
        if (isCorrect) {
            DDLogVerbose(@"[BLEDemo] Lock succeed");
        }else {
            DDLogError(@"[BLEDemo] Lock failed");
        }
    }];
}

- (IBAction)ble_scanNearby:(id)sender {//设置查找设备的过滤器
    [BCLBle scan:CHANNELID block:^(CBCentralManager * _Nonnull central, CBPeripheral * _Nonnull peripheral, NSDictionary * _Nonnull advertisementData, NSNumber * _Nonnull RSSI) {
        DDLogDebug(@"[BLEDemo] Device found: %@\nExtended data: %@",peripheral.name,advertisementData);
        NSLog(@"[BLEDemo] Device found: %@\nExtended data: %@",peripheral.name,advertisementData);
    }];
}

- (IBAction)ble_lockStatus:(id)sender {
    [BCLBle lockStatus:^(BOOL isCorrect, NSString * _Nonnull type) {
        if (isCorrect) {
            if ([type intValue] == 1 || [type intValue] == 2) {
                DDLogVerbose(@"[BLEDemo] current lock's status -- Unlocked");
            }else if ([type intValue] == 3) {
                DDLogVerbose(@"[BLEDemo] current lock's status -- Locked");
            }else {
                DDLogVerbose(@"[BLEDemo] current lock's status -- Unkown");
            }
        }else {
            DDLogError(@"[BLEDemo] Get current lock's status failed");
        }
    }];
}

- (IBAction)ble_lockBattery:(id)sender {
    [BCLBle lockBattery:^(BOOL isCorrect, NSString *battery) {
        if (isCorrect) {
            DDLogVerbose(@"[BLEDemo] Current lock's battery -- %@",battery?:@"Unkonw");
        }else {
            DDLogError(@"[BLEDemo] Get current lock's battery failed");
        }
    }];
}

- (IBAction)ble_addUser:(id)sender {
    DDLogVerbose(@"[BLEDemo](User add) -- %@%@",TEST_MASTERID,TEST_USERID);
    [BCLBle addUserWithMasterId:TEST_MASTERID userId:TEST_USERID startTime:nil endTime:nil times:nil pwdType:TEST_USER_TYPE pwd:TEST_USER_PWD1 block:^(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId) {
        if (isCorrect) {
            DDLogVerbose(@"[BLEDemo](User add) -- isCorrect: %@", isCorrect?@"true":@"false");
            DDLogVerbose(@"[BLEDemo](User add) -- resultInt: %d", resultInt);
            DDLogVerbose(@"[BLEDemo](User add) -- msg: %@", msg);
            DDLogVerbose(@"[BLEDemo](User add) -- superId: %@", masterId);
            DDLogVerbose(@"[BLEDemo](User add) -- userId: %@", userId);
        }else {
            DDLogError(@"[BLEDemo](User add) -- isCorrect: %@", isCorrect?@"true":@"false");
            DDLogError(@"[BLEDemo](User add) -- resultInt: %d", resultInt);
            DDLogError(@"[BLEDemo](User add) -- msg: %@", msg);
            DDLogError(@"[BLEDemo](User add) -- superId: %@", masterId);
            DDLogError(@"[BLEDemo](User add) -- userId: %@", userId);
        }
    }];
}

- (IBAction)ble_modifyUser:(id)sender {
    DDLogVerbose(@"[BLEDemo](User modify) -- %@%@",TEST_MASTERID,TEST_USERID);
    [BCLBle modifyUserWithMasterId:TEST_MASTERID userId:TEST_USERID startTime:nil endTime:nil times:nil pwdType:TEST_USER_TYPE pwd:TEST_USER_PWD2 block:^(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId) {
        if (isCorrect) {
            DDLogVerbose(@"[BLEDemo](User modify) -- isCorrect: %@", isCorrect?@"true":@"false");
            DDLogVerbose(@"[BLEDemo](User modify) -- resultInt: %d", resultInt);
            DDLogVerbose(@"[BLEDemo](User modify) -- msg: %@", msg);
            DDLogVerbose(@"[BLEDemo](User modify) -- superId: %@", masterId);
            DDLogVerbose(@"[BLEDemo](User modify) -- userId: %@", userId);
        }else {
            DDLogError(@"[BLEDemo](User modify) -- isCorrect: %@", isCorrect?@"true":@"false");
            DDLogError(@"[BLEDemo](User modify) -- resultInt: %d", resultInt);
            DDLogError(@"[BLEDemo](User modify) -- msg: %@", msg);
            DDLogError(@"[BLEDemo](User modify) -- superId: %@", masterId);
            DDLogError(@"[BLEDemo](User modify) -- userId: %@", userId);
        }
    }];
}

- (IBAction)ble_delUser:(id)sender {
    DDLogVerbose(@"[BLEDemo](User delete) -- %@%@",TEST_MASTERID,TEST_USERID);
    [BCLBle deleteUserWithMasterId:TEST_MASTERID userId:TEST_USERID block:^(BOOL isCorrect, int resultInt, NSString *msg, NSString *masterId, NSString *userId) {
        if (isCorrect) {
            DDLogVerbose(@"[BLEDemo](User delete) -- isCorrect: %@", isCorrect?@"true":@"false");
            DDLogVerbose(@"[BLEDemo](User delete) -- resultInt: %d", resultInt);
            DDLogVerbose(@"[BLEDemo](User delete) -- msg: %@", msg);
            DDLogVerbose(@"[BLEDemo](User delete) -- superId: %@", masterId);
            DDLogVerbose(@"[BLEDemo](User delete) -- userId: %@", userId);
        }else {
            DDLogError(@"[BLEDemo](User delete) -- isCorrect: %@", isCorrect?@"true":@"false");
            DDLogError(@"[BLEDemo](User delete) -- resultInt: %d", resultInt);
            DDLogError(@"[BLEDemo](User delete) -- msg: %@", msg);
            DDLogError(@"[BLEDemo](User delete) -- superId: %@", masterId);
            DDLogError(@"[BLEDemo](User delete) -- userId: %@", userId);
        }
    }];
}

- (IBAction)log_clear:(UIButton *)sender {
    self.logTV.text = @"";
//    [self.logTV setAttributedText:@""];
}

- (NSString *)timeStampToFormat:(NSString *)format date:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format?:@"YYYY-MM-dd HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate: date];
    return dateString;
}

- (void)logMessage:(DDLogMessage *)logMessage
{
    UIColor *color = [UIColor blackColor];
    switch (logMessage.flag) {
        case DDLogFlagDebug:
        {
            color = UIColor.blackColor;
        }
            break;
        case DDLogFlagError:
        {
            color = UIColor.redColor;
        }
            break;
        case DDLogFlagWarning:
        {
            color = UIColor.orangeColor;
        }
            break;
        case DDLogFlagInfo:
        {

        }
            break;
        case DDLogFlagVerbose:
        {
            color = [BCLUtil bcl_colorWithHex:0x007B43];
        }
            break;
        default:
            break;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableAttributedString *mutableText = [self.logTV.attributedText mutableCopy];
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n[%@]%@",[self timeStampToFormat:nil date:logMessage.timestamp],logMessage.message]];
        [text addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, text.string.length)];
        [mutableText appendAttributedString:[text copy]];
        [self.logTV setAttributedText:[mutableText copy]];
        [self.logTV scrollRangeToVisible:NSMakeRange(self.logTV.text.length - 1, 1)];

        self.statusL.text = logMessage.message;
    });
}

@synthesize logFormatter;

@end
