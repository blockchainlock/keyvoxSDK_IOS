//
//  BCLUtil.m
//  BCLBleModule-Tag
//
//  Created by pantao on 2021/3/12.
//  Copyright © 2021 guxs1129@163.com. All rights reserved.
//

#import "BCLUtil.h"

@implementation BCLUtil

+ (UIColor *)bcl_colorWithHex:(long)hexColor
{
    CGFloat red = ((CGFloat)((hexColor & 0xFF0000) >> 16))/255.0;
    CGFloat green = ((CGFloat)((hexColor & 0xFF00) >> 8))/255.0;
    CGFloat blue = ((CGFloat)(hexColor & 0xFF))/255.0;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1];
}

@end
