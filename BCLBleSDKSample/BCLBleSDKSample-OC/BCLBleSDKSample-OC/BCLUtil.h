//
//  BCLUtil.h
//  BCLBleModule-Tag
//
//  Created by pantao on 2021/3/12.
//  Copyright © 2021 guxs1129@163.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCLUtil : NSObject

+ (UIColor *)bcl_colorWithHex:(long)hexColor;

@end

NS_ASSUME_NONNULL_END
