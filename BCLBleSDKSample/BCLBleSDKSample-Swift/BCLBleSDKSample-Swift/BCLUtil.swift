//
//  BCLUtil.swift
//  BCLBleSDKSample-Swift
//
//  Created by pantao on 2021/3/16.
//

import Foundation

class BCLUtil: NSObject {
    static func bcl_colorWithHex(hexColor: Int) -> UIColor {
        let red = ((CGFloat)((hexColor & 0xFF0000) >> 16))/255.0;
        let green = ((CGFloat)((hexColor & 0xFF00) >> 8))/255.0;
        let blue = ((CGFloat)(hexColor & 0xFF))/255.0;
        return UIColor.init(red: red, green: green, blue: blue, alpha: 1);
    }
}
