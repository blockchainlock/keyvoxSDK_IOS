//
//  SampleViewController.swift
//  BCLBleSDKSample-Swift
//
//  Created by pantao on 2021/3/16.
//

import Foundation
import CoreBluetooth
import CocoaLumberjack

class SampleViewController: UIViewController, DDLogger {
    let CHANNELID = "channel_test";
    let TEST_MASTERID = "00"
    let TEST_USERID = "11"
    let TEST_USER_TYPE = 5
    let TEST_USER_PWD1 = "0001"
    let TEST_USER_PWD2 = "66666666"

    @IBOutlet weak var macTF: UITextField!
    @IBOutlet weak var idTF: UITextField!
    @IBOutlet weak var pwdTF: UITextField!
    @IBOutlet weak var logTV: UITextView!
    @IBOutlet weak var statusL: UILabel!
    
    var _peripheral: CBPeripheral? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        logTV.isEditable = false
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        DDLog.add(self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ble_connected), name: NSNotification.Name(rawValue: BCLBLE_NOTIFICATION_CONNECTED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ble_disconnected), name: NSNotification.Name(rawValue: BCLBLE_NOTIFICATION_DISCONNECTED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ble_connectfailed), name: NSNotification.Name(rawValue: BCLBLE_NOTIFICATION_CONNECTFAILED), object: nil)
    }
    
    @objc func ble_connected(n: Notification) {
        let obj: NSDictionary? = n.object as? NSDictionary
        let peripheral: CBPeripheral? = obj?.object(forKey: "peripheral") as? CBPeripheral
        if peripheral != nil {
            DDLogVerbose("[BLEDemo] \(peripheral!.name ?? "Unkonw device"): " + "连接成功");
            self.view.backgroundColor = BCLUtil.bcl_colorWithHex(hexColor: 0x88cb7f)
        }
        
        if _peripheral != nil && peripheral != nil && !_peripheral!.isEqual(peripheral!) {
            DDLogError("[BLEDemo] \(peripheral?.name ?? "Unkonw device"): " + "连接成功的不是当前要连接的peripheral")
        }
    }
    
    @objc func ble_disconnected(n: Notification) {
        let obj: NSDictionary? = n.object as? NSDictionary
        if obj != nil {
            let peripheral: CBPeripheral? = obj!.object(forKey: "peripheral") as? CBPeripheral
            if peripheral != nil {
                DDLogError("[BLEDemo] \(peripheral!.name ?? "Unkonw"): " + "连接断开")
                self.view.backgroundColor = BCLUtil.bcl_colorWithHex(hexColor: 0xa25768)
            }
        }
    }
    
    @objc func ble_connectfailed(n: Notification) {
        let obj: NSDictionary? = n.object as? NSDictionary
        if obj != nil {
            let peripheral: CBPeripheral? = obj!.object(forKey: "peripheral") as? CBPeripheral
            if peripheral != nil {
                DDLogError("[BLEDemo] \(peripheral!.name ?? "Unkonw"): " + "连接失败")
                self.view.backgroundColor = BCLUtil.bcl_colorWithHex(hexColor: 0x88cb7f)
            }
        }
    }
    
    @IBAction func ble_scan(_ sender: Any) {
        DDLogDebug("[BLEDemo] Scanning...");
        NSLog("[BLEDemo] Scanning...")
        
        var macInput = macTF.text
        macInput = macInput?.replacingOccurrences(of: " ", with: "")
        macInput = macInput?.replacingOccurrences(of: ":", with: "")
        if macInput!.count <= 0 {
            DDLogDebug("[BLEDemo] Input mac address plz")
            return
        }
        
        BCLBle.scan(CHANNELID, mac: macInput!) { [self] (central, peripheral, advertisementData, RSSI) in
            DDLogVerbose("[BLEDemo] Scaned mac address: \(macInput!) (RSSI: \(RSSI)")
            NSLog("[BLEDemo] Scaned mac address: \(macInput!) (RSSI: \(RSSI)")
            DDLogVerbose("[BLEDemo] Device found: \(peripheral.name ?? "Unkonw")\nExtended data: \(advertisementData)")
            NSLog("[BLEDemo] Device found: \(peripheral.name ?? "Unkonw")\nExtended data: \(advertisementData)")
            BCLBle.scanCancel()
            _peripheral = peripheral
        }
    }
    
    @IBAction func ble_stopscan(_ sender: Any) {
        BCLBle.scanCancel()
    }
    
    @IBAction func ble_connect(_ sender: Any) {
        DDLogDebug("[BLEDemo] Connecting...")
        
        var macInput = macTF.text
        macInput = macInput?.replacingOccurrences(of: " ", with: "")
        macInput = macInput?.replacingOccurrences(of: ":", with: "")
        if macInput!.count <= 0 {
            DDLogDebug("[BLEDemo] Input mac address plz")
            return
        }
        
        BCLBle.connect(CHANNELID, peripheral: _peripheral) { (central, peripheral) in
            if peripheral != nil {
                BCLBle.onDiscoverCharacteristics(withChannel: self.CHANNELID, targetPeripheral: self._peripheral!) {
                    
                }
            }
        }
    }
    
    @IBAction func ble_disconnect(_ sender: Any) {
        if _peripheral == nil {
            return
        }
        BCLBle.disConnect(with: _peripheral!)
    }
    
    @IBAction func ble_login(_ sender: Any) {
        if idTF.text == nil || idTF.text?.count == 0 {
            DDLogError("[BLEDemo] Please type in master id")
            return
        }
        if pwdTF.text == nil || pwdTF.text?.count == 0 {
            DDLogError("[BLEDemo] Please type in password")
            return
        }
        DDLogDebug("[BLEDemo] Logining...");
        
        var macInput = macTF.text
        macInput = macInput?.replacingOccurrences(of: " ", with: "")
        macInput = macInput?.replacingOccurrences(of: ":", with: "")
        if macInput!.count <= 0 {
            DDLogDebug("[BLEDemo] Input mac address plz")
            return
        }
        BCLBle.login(CHANNELID, mac: macInput!, peripheral: _peripheral, masterId: idTF.text!, pwd: pwdTF.text!) { (logined, peripheral, channel, timeout, masterId, userId) in
            self._peripheral = peripheral
            if logined {
                DDLogVerbose("[BLEDemo] Login succeed")
                DDLogVerbose("[BLEDemo] Login succeed masterId: \(masterId)");
                DDLogVerbose("[BLEDemo] Login succeed userId: \(userId)");
            }else {
                if timeout {
                    DDLogError("[BLEDemo] Login timeout")
                }else {
                    DDLogError("[BLEDemo] Login failed")
                }
            }
        }
    }
    
    @IBAction func ble_open(_ sender: Any) {
        BCLBle.unlock(CHANNELID) { (isCorrect, key, timeout) in
            if isCorrect {
                DDLogVerbose("[BLEDemo] Unlock succeed");
            }else {
                DDLogError("[BLEDemo] Unlock failed");
            }
        }
    }
    
    @IBAction func ble_close(_ sender: Any) {
        BCLBle.lock(CHANNELID) { (isCorrect, key, timeout) in
            if isCorrect {
                DDLogVerbose("[BLEDemo] Lock succeed");
            }else {
                DDLogError("[BLEDemo] Lock failed");
            }
        }
    }
    
    @IBAction func ble_lockStatus(_ sender: Any) {
        BCLBle.lockStatus { (isCorrect, type: String!) in
            if isCorrect {
                if Int(type) == 1 || Int(type) == 2 {
                    DDLogVerbose("[BLEDemo] current lock's status -- Unlocked")
                }else if Int(type) == 3 {
                    DDLogVerbose("[BLEDemo] current lock's status -- Locked")
                }else {
                    DDLogVerbose("[BLEDemo] current lock's status -- Unkown")
                }
            }else {
                DDLogError("[BLEDemo] Get current lock's status failed")
            }
        }
    }
    
    @IBAction func ble_lockBattery(_ sender: Any) {
        BCLBle.lockBattery { (isCorrect, battery) in
            if isCorrect {
                DDLogVerbose("[BLEDemo] Current lock's battery -- \(battery)")
            }else {
                DDLogError("[BLEDemo] Get current lock's battery failed")
            }
        }
    }
    
    @IBAction func ble_scanNearby(_ sender: Any) {
    }
    
    @IBAction func ble_addUser(_ sender: Any) {
        DDLogVerbose("[BLEDemo](User add) -- \(TEST_MASTERID)\(TEST_USERID)");
        BCLBle.addUser(withMasterId: TEST_MASTERID, userId: TEST_USERID, startTime: nil, endTime: nil, times: nil, pwdType: TEST_USER_TYPE, pwd: TEST_USER_PWD1) { (isCorrect, resultInt, msg, masterId, userId) in
            if isCorrect {
                DDLogVerbose("[BLEDemo](User add) -- isCorrect: \(isCorrect ? "true" : "false")")
                DDLogVerbose("[BLEDemo](User add) -- resultInt: \(resultInt)")
                DDLogVerbose("[BLEDemo](User add) -- msg: \(msg)")
                DDLogVerbose("[BLEDemo](User add) -- superId: \(masterId)")
                DDLogVerbose("[BLEDemo](User add) -- userId: \(userId)")
            }else {
                DDLogError("[BLEDemo](User add) -- isCorrect: \(isCorrect ? "true" : "false")")
                DDLogError("[BLEDemo](User add) -- resultInt: \(resultInt)")
                DDLogError("[BLEDemo](User add) -- msg: \(msg)")
                DDLogError("[BLEDemo](User add) -- superId: \(masterId)")
                DDLogError("[BLEDemo](User add) -- userId: \(userId)")
            }
        }
    }
    
    @IBAction func ble_modifyUser(_ sender: Any) {
        DDLogVerbose("[BLEDemo](User modify) -- \(TEST_MASTERID)\(TEST_USERID)");
        BCLBle.modifyUser(withMasterId: TEST_MASTERID, userId: TEST_USERID, startTime: nil, endTime: nil, times: nil, pwdType: TEST_USER_TYPE, pwd: TEST_USER_PWD2) { (isCorrect, resultInt, msg, masterId, userId) in
            if isCorrect {
                DDLogVerbose("[BLEDemo](User modify) -- isCorrect: \(isCorrect ? "true" : "false")")
                DDLogVerbose("[BLEDemo](User modify) -- resultInt: \(resultInt)")
                DDLogVerbose("[BLEDemo](User modify) -- msg: \(msg)")
                DDLogVerbose("[BLEDemo](User modify) -- superId: \(masterId)")
                DDLogVerbose("[BLEDemo](User modify) -- userId: \(userId)")
            }else {
                DDLogError("[BLEDemo](User modify) -- isCorrect: \(isCorrect ? "true" : "false")")
                DDLogError("[BLEDemo](User modify) -- resultInt: \(resultInt)")
                DDLogError("[BLEDemo](User modify) -- msg: \(msg)")
                DDLogError("[BLEDemo](User modify) -- superId: \(masterId)")
                DDLogError("[BLEDemo](User modify) -- userId: \(userId)")
            }
        }
    }
    
    @IBAction func ble_delUser(_ sender: Any) {
        DDLogVerbose("[BLEDemo](User delete) -- \(TEST_MASTERID)\(TEST_USERID)");
        BCLBle.deleteUser(withMasterId: TEST_MASTERID, userId: TEST_USERID) { (isCorrect, resultInt, msg, masterId, userId) in
            if isCorrect {
                DDLogVerbose("[BLEDemo](User delete) -- isCorrect: \(isCorrect ? "true" : "false")")
                DDLogVerbose("[BLEDemo](User delete) -- resultInt: \(resultInt)")
                DDLogVerbose("[BLEDemo](User delete) -- msg: \(msg)")
                DDLogVerbose("[BLEDemo](User delete) -- superId: \(masterId)")
                DDLogVerbose("[BLEDemo](User delete) -- userId: \(userId)")
            }else {
                DDLogError("[BLEDemo](User delete) -- isCorrect: \(isCorrect ? "true" : "false")")
                DDLogError("[BLEDemo](User delete) -- resultInt: \(resultInt)")
                DDLogError("[BLEDemo](User delete) -- msg: \(msg)")
                DDLogError("[BLEDemo](User delete) -- superId: \(masterId)")
                DDLogError("[BLEDemo](User delete) -- userId: \(userId)")
            }
        }
    }
    
    @IBAction func log_clear(_ sender: Any) {
    }
    
    func timeStamp(format: String?, date: NSDate?) -> String {
        if date == nil {
            return ""
        }
        let formatter = DateFormatter.init()
        formatter.dateFormat = format ?? "YYYY-MM-dd HH:mm:ss"
        let dateString: String = formatter.string(from: date! as Date)
        return dateString
    }
    
    func log(message logMessage: DDLogMessage) {
        var color = UIColor.black
        switch logMessage.flag {
            case DDLogFlag.debug:
                color = UIColor.black
                break
            case DDLogFlag.error:
                color = UIColor.red
                break
            case DDLogFlag.warning:
                color = UIColor.orange
                break
            case DDLogFlag.info:
                break
            case DDLogFlag.verbose:
                color = BCLUtil.bcl_colorWithHex(hexColor: 0x007B43)
                break
            default:
                color = UIColor.black
                break
        }
        DispatchQueue.main.async {
            let mutableText: NSMutableAttributedString = self.logTV.attributedText.mutableCopy() as! NSMutableAttributedString
            let text = NSMutableAttributedString.init(string: "\n[\(self.timeStamp(format: nil, date: logMessage.timestamp as NSDate))]\(logMessage.message)")
            text.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(0, text.string.count))
            mutableText.append(text.copy() as! NSAttributedString)
            self.logTV.attributedText = (mutableText.copy() as! NSAttributedString)
            self.logTV.scrollRangeToVisible(NSMakeRange(self.logTV.text.count - 1, 1))
            
            self.statusL.text = logMessage.message
        }
    }
    
    var logFormatter: DDLogFormatter?
}
